#if defined(__MINGW32__)
#define __MSVCRT_VERSION__ 0x800
#define _WIN32_WINNT 0x0500
#endif

#include<stdio.h>
#include<stdlib.h>
#include<stdint.h>
#include <windows.h>
#include <commctrl.h>
#include <conio.h>
#include <GL/freeglut.h>
#include "zlib/include/zlib.h"
#include "zlib/include/zconf.h"
#include "CImg.h"

#define CHUNK 8192

//#define _WIN32_IE 0x0900


#if defined(MSDOS) || defined(OS2) || defined(WIN32) || defined(__CYGWIN__)
#  include <fcntl.h>
#  include <io.h>
#  define SET_BINARY_MODE(file) setmode(fileno(file), O_BINARY)
#else
#  define SET_BINARY_MODE(file)
#endif

using namespace cimg_library;

void DrawScene(void);
void InitOpenGL(void);
void ReshapeWindow(int width, int height);
void createGLUTMenus();
void processMenuEvents(int option);
int StrToInt(char *buf);
int inf(uint8_t source[]);


UINT_PTR CALLBACK hook(HWND h, UINT u, WPARAM w, LPARAM l)
{
    return FALSE;
}


int mainWindow;
int WYS;
int SZER;
int COMPBYTES;

double WSP_SZER,WSP_WYS;
uint8_t *wsk=NULL;

int main(int argc, char **argv)
{

    /*uint8_t head[8];
    //uint32_t bytecount[1];
    FILE *fp;
    fp=fopen("10.crd", "rb");

    fread(head,1,8,fp);

    SZER=head[0]*100+head[1];
    WYS=head[2]*100+head[3];
    COMPBYTES= (int)head[4]<<24 | (int)head[5]<<16 | (int)head[6]<<8 | (int)head[7];

    printf("Byty: %d \n",COMPBYTES);
    printf("%d , %d \n",SZER,WYS);

    uint8_t *t= new uint8_t[COMPBYTES];
    fread(t,1,COMPBYTES,fp);

    inf(t);

    fclose(fp);*/



	glutInit(&argc, argv);
    glutInitDisplayMode(GLUT_RGB | GLUT_SINGLE);
    glutInitWindowSize(800, 600);
	glutInitWindowPosition(150,150);
    mainWindow = glutCreateWindow("Interpreter Y");

	if(mainWindow == 0){
		puts("Nie mozna stworzyc okna!!!\nWyjscie z programu.\n");
		exit(-1);
	}



	// Czynimy aktywnym okno główne programu
	glutSetWindow(mainWindow);

	// Tutaj rejestrujemy funkcje narzędziowe - tzw. callbacks
	glutDisplayFunc(DrawScene);
	glutReshapeFunc(ReshapeWindow);

	createGLUTMenus();

	// ustawienia poczštkowe
	InitOpenGL();

	// Wejcie do pętli programu
	glutMainLoop();

	return(0);
}

int StrToInt(char *buf){
int liczb=0,i,poz;

if(buf[0]==45)
{
       poz=1;
       for(i=3;i>=0;i--)
       if(buf[i]>47){
        liczb+=(buf[i]-48)*poz;
        poz*=10;
       }
       return liczb*(-1);
}
else
    {
    poz=1;
       for(i=3;i>=0;i--)
       if(buf[i]>47){
        liczb+=(buf[i]-48)*poz;
        poz*=10;
       }
    return liczb;
   }
}

void DrawScene(void)
{
    glClear(GL_COLOR_BUFFER_BIT);
    glColor3f(0.0f,0.0f,0.0f);

if(wsk!=NULL){

double i,j,col,wsp_k;
int c;
wsp_k=1.0/255;


	glBegin(GL_POINTS);

    c=0;
	for(j=100;j>-100;j-=WSP_WYS)
	for(i=-100;i<100;i+=WSP_SZER){
    c++;
    if(c>WYS*SZER)
        break;
    col=wsk[c]*wsp_k;
    glColor3f(col,col,col);
    glVertex2f(i, j);
	}

	glEnd();
    glFlush();
}
}

void InitOpenGL(void)
{
	// Usawiamy domylny, czarny kolor tła okna - bufor ramki malujemy na czarno
	glClearColor(0.0f, 0.0f, 0.0f, 1.0f);
}

/* Tę funkcję wywołuje system w momencie gdy uytkownik zmieni myszš
 * rozmiar głownego okna. jej zadaniem jest zachowanie propocji wymiarów
 * rysowanych obiektów niezależnie od wymiarów okna.
 */
void ReshapeWindow(int width, int height)
{
	int aspectRatio; // = width / height

	// Na wypadek dzielenia przez 0
	if(height == 0) height = 1;

	// Wyliczamy współczynnik proporcji
	aspectRatio = width / height;

	// Ustawiamy wielkoci okna okna urzšdzenia w zakresie
	// od 0,0 do wysokoć, szerokoć
	glViewport(0, 0, width, height);

	// Ustawiamy układ współrzędnych obserwatora
    glMatrixMode(GL_PROJECTION);

	// Resetujemy macierz projkecji
    glLoadIdentity();

	// Korekta
    if(width <= height)
		glOrtho(-100.0, 100.0, -100.0/aspectRatio, 100.0/aspectRatio, 1.0, -1.0);
     else
		glOrtho(-100.0*aspectRatio, 100.0*aspectRatio, -100.0, 100.0, 1.0, -1.0);

	// Ustawiamy macierz modelu
    glMatrixMode(GL_MODELVIEW);

	// Resetujemy macierz modelu
    glLoadIdentity();

}

void createGLUTMenus() {

	int menu;
	menu = glutCreateMenu(processMenuEvents);

	glutAddMenuEntry("Open",1);
	glutAddMenuEntry("Save",2);

	glutAttachMenu(GLUT_RIGHT_BUTTON);
}

void processMenuEvents(int option) {

	switch (option) {
    case 1 :
        {
        char fname[256];
        fname[0] = '\0';
        OPENFILENAME ofn;
        ZeroMemory(&ofn, sizeof(ofn));
        ofn.lStructSize = sizeof(ofn);
        ofn.lpstrFile = fname;
        ofn.nMaxFile = sizeof(fname);
        ofn.Flags = OFN_ENABLEHOOK | OFN_EXPLORER | OFN_FILEMUSTEXIST;
        ofn.lpfnHook = hook;
        ofn.lpstrFilter="Pliki Credo (*.crd)\0*.crd\0Wszystkie pliki\0*.*\0";
        ofn.lpstrDefExt = "crd";
        GetOpenFileName(&ofn);

        FILE *fp;
        uint8_t head[8];

        fp=fopen(fname, "rb");

        fread(head,1,8,fp);

        SZER=head[0]*100+head[1];
        WYS=head[2]*100+head[3];
        COMPBYTES= (int)head[4]<<24 | (int)head[5]<<16 | (int)head[6]<<8 | (int)head[7];

        WSP_SZER=(double)200/SZER;
        WSP_WYS=(double)200/WYS;

        printf("Byty: %d \n",COMPBYTES);
        printf("%d , %d \n",SZER,WYS);

        uint8_t *t= new uint8_t[COMPBYTES];
        fread(t,1,COMPBYTES,fp);

        inf(t);

        delete[] t;
        fclose(fp);
        }
            break;

    case 2 :
        if(wsk!=NULL)
        {
        CImg<uint8_t> img(wsk, SZER, WYS,1,1);
        img.save_bmp("sav.bmp");
        printf("Zapisano\n");
        }
            break;

	}
}

int inf(uint8_t source[])
{
    int ret,i,n=0,o=0;
    unsigned have;
    z_stream strm;
    uint8_t in[CHUNK];
    uint8_t out[CHUNK];
    int bytecnt = COMPBYTES;

    int rozmiary=WYS*SZER;

    strm.zalloc = Z_NULL;
    strm.zfree = Z_NULL;
    strm.opaque = Z_NULL;
    strm.avail_in = 0;
    strm.next_in = Z_NULL;
    ret = inflateInit(&strm);
    if (ret != Z_OK)
        return ret;

    uint8_t *destination = new uint8_t[rozmiary];

    do {

        if(bytecnt>CHUNK){
        strm.avail_in = CHUNK;
            for(i=0;i<CHUNK;i++)
                in[i]=source[n+i];
                n+=CHUNK;
        }
        else{
        strm.avail_in=bytecnt;
            for(i=0;i<bytecnt;i++)
                in[i]=source[n+i];
        }
        bytecnt -= CHUNK;

        strm.next_in = in;

        do {
            strm.avail_out = CHUNK;
            strm.next_out = out;
            ret = inflate(&strm, Z_NO_FLUSH);
            //printf(" Inter %d ",strm.avail_out);
            have = CHUNK - strm.avail_out;

            for(i=0;i<have;i++)
                destination[o+i]=out[i];

            o+=have;
        } while (strm.avail_out == 0);

    } while (ret != Z_STREAM_END);
    wsk=destination;

    (void)inflateEnd(&strm);
    return ret == Z_STREAM_END ? Z_OK : Z_DATA_ERROR;
}


